package br.com.centralsigma.model;

import java.util.Date;

/**
 * Created by Fernando Esmaniotto on 12/12/2017.
 */

public class Notificacao {

    private Double valor;
    private String horario;
    private boolean alertar;

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public boolean isAlertar() {
        return alertar;
    }

    public void setAlertar(boolean alertar) {
        this.alertar = alertar;
    }
}
