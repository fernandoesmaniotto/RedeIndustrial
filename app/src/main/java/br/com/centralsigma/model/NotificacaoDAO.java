package br.com.centralsigma.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.centralsigma.database.ConnectionDB;

/**
 * Created by Fernando Esmaniotto on 12/12/2017.
 */

public class NotificacaoDAO {

    private static ConnectionDB connectionDB = null;
    private static SQLiteDatabase db = null;

    public NotificacaoDAO(Context ctx){
        connectionDB = new ConnectionDB(ctx);
        db = connectionDB.getWritableDatabase();
    }

    public void salvarNotificacao(Notificacao notificacao){
        ContentValues values = new ContentValues();

        values.put("VALOR", notificacao.getValor());
        values.put("HORARIO", notificacao.getHorario());
        values.put("ALERTAR",  notificacao.isAlertar());

        db.insert("NOTIFICACAO", null, values);
        db.close();
    }

    public List<Notificacao> listarNotificacao(){
        Cursor cursor = null;
        List<Notificacao> notificacoes = new ArrayList<Notificacao>();
        Notificacao notificacao = null;
        try {
            cursor = db.rawQuery("SELECT * FROM NOTIFICACAO", null);
            while (cursor.moveToNext()) {
                notificacao = new Notificacao();
                notificacao.setValor(cursor.getDouble(cursor.getColumnIndex("VALOR")));
                notificacao.setHorario(cursor.getString(cursor.getColumnIndex("HORARIO")));
                notificacoes.add(notificacao);
            }
        } finally {
            if(cursor != null)
                cursor.close();
        }

        return notificacoes;
    }
}
