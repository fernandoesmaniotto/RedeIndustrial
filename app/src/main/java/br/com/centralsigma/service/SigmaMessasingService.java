package br.com.centralsigma.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import java.util.Date;
import java.util.Map;

import br.com.centralsigma.R;
import br.com.centralsigma.controller.MainActivity;
import br.com.centralsigma.database.ConnectionDB;
import br.com.centralsigma.model.Notificacao;
import br.com.centralsigma.model.NotificacaoDAO;
import br.com.centralsigma.ui.NotificacaoAdapter;

/**
 * Created by Fernando Esmaniotto on 12/12/2017.
 */

public class SigmaMessasingService extends FirebaseMessagingService {

    private Notificacao notificacao = new Notificacao();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            Map<String, String> data = remoteMessage.getData();
            notificacao.setValor(Double.parseDouble(data.get("valor")));
            notificacao.setHorario(data.get("horario"));
            notificacao.setAlertar(Boolean.valueOf(data.get("alertar")));
        }

        notificacaoFirebase(notificacao);
    }

    //Apresenta a notificação para o usuário
    public void notificacaoFirebase(Notificacao notification) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isNotificacao = preferences.getBoolean("notificacao", true);

        if (notification.isAlertar() && isNotificacao){
            String mensagem =  String.valueOf("Valor: "+notification.getValor());

            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(getApplicationContext());
            Notification notificacao = builder.setContentTitle(getString(R.string.titulo))
                    .setContentText(mensagem).setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                    .build();

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notificacao);
        }


        NotificacaoDAO dao = new NotificacaoDAO(this.getApplicationContext());
        dao.salvarNotificacao(notification);

        Intent intent = new Intent("BROADCAST_UPDATE");
        sendBroadcast(intent);
    }
}