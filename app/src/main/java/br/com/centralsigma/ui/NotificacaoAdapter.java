package br.com.centralsigma.ui;

/**
 * Created by Fernando Esmaniotto on 29/07/2017.
 */

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.centralsigma.R;
import br.com.centralsigma.model.Notificacao;

public class NotificacaoAdapter extends ArrayAdapter<Notificacao> {

    private Context context;
    private int layoutResourceId;
    private List<Notificacao> data = new ArrayList<Notificacao>();

    public NotificacaoAdapter(Context context, int layoutResourceId, List<Notificacao> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        OcorrenciaHolder holder = null;

        if(row == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new OcorrenciaHolder();
            holder.txtValor = (TextView)row.findViewById(R.id.txt_valor);
            holder.txtHorario = (TextView)row.findViewById(R.id.txt_horario);
            row.setTag(holder);
        }
        else {
            holder = (OcorrenciaHolder)row.getTag();
        }

        final Notificacao notificacao = data.get(position);
        holder.txtValor.setText("Valor: "+notificacao.getValor().toString());
        holder.txtHorario.setText("Horário: "+notificacao.getHorario());

        return row;
    }

    static class OcorrenciaHolder {
        TextView txtValor;
        TextView txtHorario;
    }
}