package br.com.centralsigma.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Fernando Esmaniotto on 09/07/2017.
 */

public class ConnectionDB extends SQLiteOpenHelper {

    private static final String DOUBLE_TYPE = " DOUBLE";
    private static final String BOOL_TYPE = " BOOLEAN";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";


   private static final String CREATE_NOTIFICACAO =
            "CREATE TABLE NOTIFICACAO (" +
                    " ID INTEGER PRIMARY KEY," +
                    " VALOR" + DOUBLE_TYPE + COMMA_SEP +
                    " HORARIO" + TEXT_TYPE + COMMA_SEP +
                    " ALERTAR" + BOOL_TYPE + " )";

    private static final String SQL_DELETE_NOTIFICACAO =
            "DROP TABLE IF EXISTS NOTIFICACAO ";


    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "sigma.db";

    public ConnectionDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_NOTIFICACAO);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_NOTIFICACAO);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}