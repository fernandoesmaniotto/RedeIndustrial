package br.com.centralsigma.controller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessaging;

import br.com.centralsigma.R;
import br.com.centralsigma.database.ConnectionDB;
import br.com.centralsigma.model.Notificacao;
import br.com.centralsigma.model.NotificacaoDAO;
import br.com.centralsigma.ui.NotificacaoAdapter;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth autoriza;
    private ListView listagemNotificacao;
    private BroadcastReceiver backgroundAtalizaListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listagemNotificacao = ( ListView )findViewById(R.id.list_notificacoes);

        //Conexão com o banco de dados
        ConnectionDB connectionDB = new ConnectionDB(this);
        SQLiteDatabase db = connectionDB.getWritableDatabase();

        //Inscrição no tópico teste
        FirebaseMessaging.getInstance().subscribeToTopic("teste");

        //Autenticação anonima no Firebase
        autoriza = FirebaseAuth.getInstance();
        autoriza.signInAnonymously()
        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser user = autoriza.getCurrentUser();
                } else {
                    Toast.makeText(MainActivity.this, getString(R.string.falha),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Serviço para atualizar do listView no momento do receibimento da notificação
        backgroundAtalizaListView = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                NotificacaoDAO dao = new NotificacaoDAO(MainActivity.this);
                NotificacaoAdapter adapter = new NotificacaoAdapter(MainActivity.this, R.layout.listview_notificacao, dao.listarNotificacao());
                listagemNotificacao.setAdapter(adapter);
            }
        };
        IntentFilter intent = new IntentFilter("BROADCAST_UPDATE");
        registerReceiver(backgroundAtalizaListView, intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        NotificacaoDAO dao = new NotificacaoDAO(this.getApplicationContext());
        NotificacaoAdapter adapter = new NotificacaoAdapter(MainActivity.this, R.layout.listview_notificacao, dao.listarNotificacao());
        listagemNotificacao.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_sair:
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(getString(R.string.aviso));
                builder.setMessage(getString(R.string.pergunta))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.sim), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton(getString(R.string.nao), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            case R.id.ic_configurar:
                Intent intent = new Intent(MainActivity.this,Preferences.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
