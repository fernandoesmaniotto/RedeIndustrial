package br.com.centralsigma.controller;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.util.Log;

import br.com.centralsigma.R;

/**
 * Created by xisberto on 08/11/14.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PreferecensFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }
}